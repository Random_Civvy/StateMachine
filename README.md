# StateMachine

A generic version of Modl. Ignore Modl. State machines are basically represented as linked lists. Not the best design but it's what I made.

Basic functionallity:

1. Build state machines by composing states together with composer states.
2. Feed state machine to a StateManager and Statemanager.Update()

## Summary

Abstract classes and interfaces for creating state machines.


Example (Should work...):

```
public class<Integer> State extends ComposableState<Integer> {
	final int internalData = 0;

	public State(int internalData) {
		this.internalData = internalData;
	}

	/**
	 * Executed when the state machine enteres this state.
	 *
	 * @param context Context data.
	 */
	public void EnterState(Integer context) {
		System.out.println(internalData + context.intValue());
	}

	/**
	 * Poll the state.
	 *
	 * @param context Context data.
	 * @return Signal for the StateManager whether it should stay on this state, move on to the next state, or stop executing its state machine.
	 */
	public StateSignal<Integer> Update(Integer context) {
		System.out.println(internalData * context.intValue());
		return new StateSignal(null, StateStatus.Change);
	}

	/**
	 * Executed when the state machine exits this state.
	 *
	 * @param context Context data.
	 */
	public void ExitState(Integer context) {
		System.out.println(internalData - context.intValue());
	}
}


// Somewhere else....
State s1 = new State(1);
State s2 = new State(2);
State s3 = new State(3);

ComposableState<Integer> head =  s1.then(
				 s2.then(
				 s3.then(
				 new End<Integer>()
				 )));

StateManager manager = new StateManager();
manager.ExternalSetState(head, ManagerStatus.RUNNING);

while (true) {
	manager.Update(new Integer(5));
}

// Should print:
// 6
// 5
// -4
// 7
// 10
// -3
// 8
// 15
// -2
```

## src/composers/

Basic classes which can combine states and predicates to make state machines.

Implemented composers: Then (basically chaining states together in a sequence), WhileDo (only execute a state if the predicate returns true).

Missing composers: If (IfElse, Else), ParallelExecute.

## src/state_machine

### ComposableState

Abstract base class for a state in a state machine. Call composer mthods such as then() and while_do() to create state machines. 

**Your states should extend this class.**

### IState

**IGNORE**

### IBool

Interface for decision-making predicates used in WhileDo composer (and potentially other composers).

### StateManager

Executes state machines and handles transitions between states. Can be configured to run State.EnterState() of the first state on initial run.

Run the state machine by calling StateManager.Update(). Only executes the current state if StateManager.managerStatus == ManagerStatus.RUNNING.

### StateStatus

Signal to the StateManager that it should stay on the current state, move to the next state, or stop executing the state machine.

### StateSignal

Composed of a StateStatus and the potential next state.
