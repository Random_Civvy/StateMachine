package composers;

import state_machine.ComposableState;
import state_machine.IState;
import state_machine.StateSignal;
import state_machine.StateStatus;

/**
 * Build end the state machine.
 */
public class End<Context> extends ComposableState<Context> {
	
	@Override
	public void EnterState(Context context) {
	}

	@Override
	public StateSignal<Context> Update(Context context) {
		return new StateSignal<Context>(null, StateStatus.End);
	}

	@Override
	public void ExitState(Context context) {
	}
}
