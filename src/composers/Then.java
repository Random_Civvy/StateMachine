package composers;

import state_machine.ComposableState;
import state_machine.IState;
import state_machine.StateSignal;
import state_machine.StateStatus;

/**
 * Build a chain of states.
 */
public class Then<Context> extends ComposableState<Context> {
	final boolean allowStateChangeOverride = false;

	IState<Context> first_state;
	
	/**
	 * Creates a chain of states.
	 *
	 * @param firstState
	 * @param then
	 */
	public Then(IState<Context> firstState, IState<Context> then) {
		if(firstState == null) {
			throw new IllegalArgumentException();
		}
		if(then == null) {
			throw new IllegalArgumentException();
		}
		
		first_state = firstState;
		next_state = then;
	}
	
	@Override
	public void EnterState(Context context) {
		first_state.EnterState(context);
	}

	@Override
	public StateSignal<Context> Update(Context context) {
		StateSignal<Context> signal = first_state.Update(context);
		if (signal.Status == StateStatus.Change) {
			if (allowStateChangeOverride) {
				return signal;
			} else {
				return new StateSignal<Context>(this.next_state);
			}
		} else if (signal.Status == StateStatus.End) {
			return new StateSignal<Context>(null, StateStatus.End);
		}
		return new StateSignal<Context>(null, StateStatus.Continue);
	}

	@Override
	public void ExitState(Context context) {
		first_state.ExitState(context);
	}
}
