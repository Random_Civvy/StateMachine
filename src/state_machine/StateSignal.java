package state_machine;

/**
 * A poor-man's ADT.
 */
public class StateSignal<Context> {
	public IState<Context> Next;
	public StateStatus Status;
	
	
	public StateSignal() {
		Next = null;
		Status = StateStatus.End;
	}
	
	
	public StateSignal(IState<Context> next) {
		Next = next;
		Status = StateStatus.Continue;
	}
	
	public StateSignal(IState<Context> next, StateStatus status) {
		Next = next;
		Status = status;
	}

	public static StateSignal<Context> end() {
		return new StateSignal<Context>(null, StateStatus.End);
	}

	public static StateSignal<Context> cont() {
		return new StateSignal<Context>(null, StateStatus.Continue);
	}
}
