package state_machine;

import composers.Then;
import composers.WhileDo;

/**
 * Base class for states in a state machine.
 */
public abstract class ComposableState<Context> implements IState<Context> {

	/**
	 * Chain a state after this state.
	 *
	 * @param next State after this state.
	 * @return State chain.
	 */
	public final Then<Context> then(IState<Context> next) {
		return new Then<Context>(this, next);
	}
	
	/**
	 * Stay in a state while an IBool predicate is true.
	 *
	 * @param predicate Returns true if the given state should be executed.
	 * @return WhileDo looping state.
	 */
	public final WhileDo<Context> while_do(IBool<Context> predicate, IState<Context> state) {
		return new WhileDo<Context>(predicate, state);
	}

	/**
	 * Executed when the state machine enteres this state.
	 *
	 * @param context Context data.
	 */
	public abstract void EnterState(Context context);

	/**
	 * Poll the state.
	 *
	 * @param context Context data.
	 * @return Signal for the StateManager whether it should stay on this state, move on to the next state, or stop executing its state machine.
	 */
	public abstract StateSignal<Context> Update(Context context);

	/**
	 * Executed when the state machine exits this state.
	 *
	 * @param context Context data.
	 */
	public abstract void ExitState(Context context);
}
