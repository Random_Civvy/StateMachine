package state_machine;

public class StateManager<Context> {

	boolean do_init = true;	//used only to call EnterState() of very first state

	protected IState<Context> current;
	ManagerStatus managerStatus;
	
	public boolean IsRunning() {
		return managerStatus == ManagerStatus.RUNNING;
	}
	
	public void ExternalSetState(IState<Context> state, ManagerStatus status) {
		current = state;
		managerStatus = status;
		do_init = true;
	}
	
	public void Start() {
		managerStatus = ManagerStatus.RUNNING;
	}
	
	/**
	 *
	 */
	public void Update(Context context) {
		if (managerStatus != ManagerStatus.RUNNING)
			return;
		
		if (do_init) {
			do_init = false;
			current.EnterState(context);
		}
		
		StateSignal<Context> next = current.Update(context);
		if(next.Status == StateStatus.Change) {
			if (next.Next == null) {
				//throw new Exception("Trying to change state but next is null.");
			}
			
			//Change state
			current.ExitState(context);
			current = next.Next;
			current.EnterState(context);
			managerStatus = ManagerStatus.RUNNING;
		
		} else if (next.Status == StateStatus.Continue) {
			// Stay on current state.
			managerStatus = ManagerStatus.RUNNING;
			
		} else if (next.Status == StateStatus.End) {
			// Exit the current state but stop executing.
			current.ExitState(context);
			managerStatus = ManagerStatus.STOP;
		}
	}
}
