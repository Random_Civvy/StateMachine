package state_machine;

public enum StateStatus {
	Continue,
	Change,
	End
}
