package state_machine;

/**
 * Common interface for predicates for decisions within the state machine.
 */
public interface IBool<Context> {
	/**
	 * Decides if something should continue.
	 */
	public boolean Continue(Context context);
}
