package state_machine;

public interface IState<Context> {
	public void EnterState(Context context);
	//UPDATE SHOULD NEVER RETURN NULL
	public StateSignal<Context> Update(Context context);
	public void ExitState(Context context);
}
